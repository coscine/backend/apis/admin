﻿using Coscine.Action;
using Coscine.Action.EventArgs;
using Coscine.Api.Admin.ParameterObjects;
using Coscine.Api.Admin.ReturnObjects;
using Coscine.ApiCommons;
using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Coscine.Logging;
using Coscine.Metadata;
using Coscine.ResourceTypes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Admin.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a user object.
    /// </summary>
    [Authorize]
    public class AdminController : Controller
    {
        private readonly RdfStoreConnector _rdfStoreConnector = new(Program.Configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));
        private readonly Authenticator _authenticator;
        private readonly string _graphUrl;
        private readonly string _memberUrl;
        private readonly string _userUrlPrefix;
        private readonly string _roleUrl;
        private readonly string _roleUrlPrefix;
        private readonly string _adminRole;
        private readonly ResourceModel _resourceModel;
        private readonly ProjectModel _projectModel;
        private readonly ProjectQuotaModel _projectQuotaModel;
        private readonly ResourceTypeModel _resourceTypeModel;
        private readonly Emitter _emitter;
        private readonly CoscineLogger _coscineLogger;

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public AdminController(ILogger<AdminController> logger)
        {
            _authenticator = new Authenticator(this, Program.Configuration);
            _graphUrl = "https://ror.org/04xfq0f34/roles";
            _memberUrl = "http://www.w3.org/ns/org#member";
            _userUrlPrefix = "https://coscine.rwth-aachen.de/u/";
            _roleUrl = "http://www.w3.org/ns/org#role";
            _roleUrlPrefix = "https://purl.org/coscine/terms/role#";
            _adminRole = "supportAdmin";
            _resourceModel = new ResourceModel();
            _projectModel = new ProjectModel();
            _projectQuotaModel = new ProjectQuotaModel();
            _resourceTypeModel = new ResourceTypeModel();
            _emitter = new Emitter(Program.Configuration);
            _coscineLogger = new CoscineLogger(logger);
        }

        /// <summary>
        /// Find the project related to the projectString(GUID or slug)
        /// </summary>
        /// <param name="projectString">The project id (GUID) or slug (from URL).</param>
        /// <returns>JSON list of all quotas.</returns>
        [HttpGet("[controller]/{projectString}")]
        public ActionResult<AdminProjectObject> GetProject(string projectString)
        {
            var user = _authenticator.GetUser();
            if (!HasRole(user.Id.ToString(), _adminRole))
            {
                return Unauthorized($"User does not have the role \"{_adminRole}\".");
            }

            Project project;

            if (Guid.TryParse(projectString, out Guid guid))
            {
                project = _projectModel.GetById(guid);
            }
            else
            {
                project = _projectModel.GetBySlug(projectString);
            }

            if (project == null)
            {
                return NotFound("Project was not found.");
            }

            var listOfResources = ResourceTypeFactory.Instance.GetSpecificResourceTypes().Select(x => x.SpecificTypeName);

            var allResourceTypeIds = _resourceTypeModel.GetAllWhere((resourceType) => listOfResources.Contains(resourceType.SpecificType)).Select(e => e.Id).ToList();
            var quotas = _projectQuotaModel.GetAllWhere(pq =>
              pq.ProjectId == project.Id &&
              allResourceTypeIds.Contains(pq.ResourceTypeId)
            ).ToList();

            return new AdminProjectObject
            {
                Id = project.Id,
                ProjectName = project.ProjectName,
                DisplayName = project.DisplayName,
                Quotas = quotas.Select(x => CreateAdminQuotaReturnObject(x, project.Id)).ToList(),
            };
        }

        /// <summary>
        /// Update a project maximum and allocated quota
        /// </summary>
        /// <param name="projectId">Id of the project</param>
        /// <param name="resourceTypeId">Id of the resource type</param>
        /// <param name="updateQuotaParameter">JSON object for updating the project maximum and allocated quota.</param>
        /// <returns>NoContent (204) on success.</returns>
        [HttpPut("[controller]/{projectId}/{resourceTypeId}")]
        public IActionResult UpdateQuota(Guid projectId, Guid resourceTypeId, [FromBody] UpdateQuotaParameterObject updateQuotaParameter)
        {
            var user = _authenticator.GetUser();
            if (!HasRole(user.Id.ToString(), _adminRole))
            {
                return Unauthorized($"User does not have the role \"{_adminRole}\".");
            }

            var reservedQuotaGiB = CalculateAllocatedForAll(_resourceTypeModel.GetById(resourceTypeId), projectId);
            if (reservedQuotaGiB > updateQuotaParameter.MaximumGiB)
            {
                return BadRequest($"Currently {reservedQuotaGiB} GB are reserved by resources in total. Can not set the new maximum quota lower than {reservedQuotaGiB}!");
            }

            var projectQuotaModel = new ProjectQuotaModel();
            var projectQuota = _projectQuotaModel.GetWhere(x => x.ProjectId == projectId && x.ResourceTypeId == resourceTypeId);

            if (projectQuota == null)
            {
                return NotFound("Quota was not found.");
            }

            projectQuota.MaxQuota = (int)updateQuotaParameter.MaximumGiB;
            projectQuota.Quota = projectQuota.MaxQuota;
            projectQuotaModel.Update(projectQuota);

            _emitter.EmitQuotaChanged(new AdminEventArgs(Program.Configuration) { ProjectId = projectQuota.ProjectId });

            if (Request.Query != null && Request.Query["noanalyticslog"] != "true")
            {
                LogAnalyticsAdminProjectQuotaChange(projectQuota.ProjectId, user);
            }

            return NoContent();
        }

        private void LogAnalyticsAdminProjectQuotaChange(Guid projectId, User user)
        {
            try
            {
                var quotas = _projectQuotaModel.GetAllWhere(x => x.ProjectId == projectId);
                var quotaObjects = quotas.Select(x => CreateAdminQuotaReturnObject(x, projectId)).ToList();

                _coscineLogger.AnalyticsLog(
                    new AnalyticsLogObject
                    {
                        Type = "Action",
                        Operation = "Admin Project Quota Change",
                        UserId = user.Id.ToString(),
                        ProjectId = projectId.ToString(),
                        QuotaSize = quotaObjects.ConvertAll(x => $"{x.ResourceType}: {x.Allocated.Value}/{x.Maximum.Value}")
                    });
            }
#pragma warning disable RCS1075 // Avoid empty catch clause that catches System.Exception.
            catch (Exception)
#pragma warning restore RCS1075 // Avoid empty catch clause that catches System.Exception.
            {
            }
        }

        /// <summary>
        /// Check if the user has a specific role.
        /// </summary>
        /// <param name="userId">Id (GUID) if the user.</param>
        /// <param name="role">The role of the user.</param>
        /// <returns>True if user has the role and false if not.</returns>
        private bool HasRole(string userId, string role)
        {
            var graph = _rdfStoreConnector.GetGraph(_graphUrl);

            // Get the subject (blank node for the specific member)
            var userTriples = graph.GetTriplesWithPredicateObject(graph.CreateUriNode(new Uri(_memberUrl)), graph.CreateUriNode(new Uri($"{_userUrlPrefix}{userId.ToUpper()}")));

            // Extract the node
            var userSubject = userTriples?.FirstOrDefault()?.Subject;
            if (userSubject == null)
            {
                return false;
            }

            // Get the role based on the blank node and compare it to the requested role
            var roleTriples = graph.GetTriplesWithSubjectPredicate(userSubject, graph.CreateUriNode(new Uri(_roleUrl)));
            return (roleTriples?.FirstOrDefault()?.Object.ToString()) == _roleUrlPrefix + role;
        }

        /// <summary>
        /// Sum up allocated quota for all resources of a given resource type within a project.
        /// </summary>
        /// <param name="resourceType">The used resource type.</param>
        /// <param name="projectId">The used project.</param>
        /// <returns>Allocated quota of the given resource type in the project.</returns>
        private long CalculateAllocatedForAll(ResourceType resourceType, Guid projectId)
        {
            var resources = _resourceModel.GetAllWhere((resource) =>
                        (from projectResource in resource.ProjectResources
                         where projectResource.ProjectId == projectId
                         select projectResource).Any() &&
                         resource.TypeId == resourceType.Id);

            var allocated = resources.Sum(resource =>
            {
                // Linked has no quota.
                var rt = ResourceTypeFactory.Instance.GetResourceType(resource);
                if (rt.GetResourceTypeInformation().Result.IsQuotaAvailable)
                {
                    return rt.GetResourceQuotaAvailable(resource.Id.ToString(), _resourceModel.GetResourceTypeOptions(resource.Id)).Result;
                }
                else
                {
                    return 0;
                }
            });

            return allocated;
        }

        /// <summary>
        /// Sum up used quota for all resources of a given resource type within a project.
        /// </summary>
        /// <param name="resourceType">The used resource type.</param>
        /// <param name="projectId">The used project.</param>
        /// <returns>Used quota of the given resource type in the project.</returns>
        private long CalculateUsedForAll(ResourceType resourceType, Guid projectId)
        {
            var resources = _resourceModel.GetAllWhere((resource) =>
                        (from projectResource in resource.ProjectResources
                         where projectResource.ProjectId == projectId
                         select projectResource).Any() &&
                         resource.TypeId == resourceType.Id);

            var used = resources.Sum(resource =>
            {
                // Linked has no quota.
                var rt = ResourceTypeFactory.Instance.GetResourceType(resource);
                if (rt.GetResourceTypeInformation().Result.IsQuotaAvailable)
                {
                    return rt.GetResourceQuotaUsed(resource.Id.ToString(), _resourceModel.GetResourceTypeOptions(resource.Id)).Result;
                }
                else
                {
                    return 0;
                }
            }
            );

            return used;
        }

        private AdminQuotaReturnObject CreateAdminQuotaReturnObject(ProjectQuota projectQuota, Guid projectId)
        {
            return new AdminQuotaReturnObject
            {
                RelationId = projectQuota.RelationId,
                ResourceType = _resourceTypeModel.GetById(projectQuota.ResourceTypeId).SpecificType,
                Allocated = new QuotaDimObject()
                {
                    Value = projectQuota.Quota,
                    Unit = QuotaUnit.GibiBYTE,
                },
                Maximum = new QuotaDimObject()
                {
                    Value = projectQuota.MaxQuota,
                    Unit = QuotaUnit.GibiBYTE,
                },
                TotalReserved = new QuotaDimObject()
                {
                    Value = CalculateAllocatedForAll(_resourceTypeModel.GetById(projectQuota.ResourceTypeId), projectId),
                    Unit = QuotaUnit.GibiBYTE,
                },
                TotalUsed = new QuotaDimObject()
                {
                    Value = CalculateUsedForAll(_resourceTypeModel.GetById(projectQuota.ResourceTypeId), projectId),
                    Unit = QuotaUnit.BYTE,
                },
            };
        }
    }
}