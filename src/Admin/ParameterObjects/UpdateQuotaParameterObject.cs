﻿using System;

namespace Coscine.Api.Admin.ParameterObjects
{
    /// <summary>
    /// Data send to update the project quota.
    /// </summary>
    public class UpdateQuotaParameterObject
    {
        /// <summary>
        /// The new maximum project quota value.
        /// </summary>
        public uint MaximumGiB { get; set; }
    }
}
