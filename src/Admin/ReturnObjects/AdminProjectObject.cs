﻿using System;
using System.Collections.Generic;

namespace Coscine.Api.Admin.ReturnObjects
{
    /// <summary>
    /// Retuned when searching for a project.
    /// Contains basic informations and quotas.
    /// </summary>
    public class AdminProjectObject
    {
        /// <summary>
        /// Project Id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Project Name (full project name)
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// Project Display Name
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// List of the admin quotas (AdminQuotaReturnObject)
        /// </summary>
        public IEnumerable<AdminQuotaReturnObject> Quotas { get; set; }
    }
}
