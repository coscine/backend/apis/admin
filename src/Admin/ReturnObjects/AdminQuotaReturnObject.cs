﻿using Coscine.Database.ReturnObjects;
using System;

namespace Coscine.Api.Admin.ReturnObjects
{
    /// <summary>
    /// Returned when searching for a project, as part of the AdminProjectObject.
    /// Contains quota informations.
    /// </summary>
    public class AdminQuotaReturnObject
    {
        /// <summary>
        /// Quota relation id (See Database, Table 'ProjectQuotas', Column 'RelationId').
        /// </summary>
        public Guid RelationId { get; set; }

        /// <summary>
        /// Which resource type the quota is referring to.
        /// </summary>
        public string ResourceType { get; set; }

        /// <summary>
        /// How much space is used by all files in all resources in total [Bytes].
        /// </summary>
        public QuotaDimObject TotalUsed { get; set; }

        /// <summary>
        /// How much space is reserved by resources in total [GiB]. Is equal to the sum of all resource quota reserved values.
        /// </summary>
        public QuotaDimObject TotalReserved { get; set; }

        /// <summary>
        /// How much space is currently allocated and is available to be taken by resources [GiB] (See Database, Table 'ProjectQuotas', Column 'Quota').
        /// </summary>
        public QuotaDimObject Allocated { get; set; }

        /// <summary>
        /// How much maximum space is possible to be taken by resources [GiB] (See Database, Table 'ProjectQuotas', Column 'MaxQuota').
        /// </summary>
        public QuotaDimObject Maximum { get; set; }

    }
}
