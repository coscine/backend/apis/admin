﻿using Coscine.ApiCommons;
using Coscine.Configuration;

namespace Coscine.Api.Admin
{

    /// <summary>
    /// Program entry class containing main.
    /// Extends AbstractProgram using ConsulConfiguration.
    /// </summary>
    public class Program : AbstractProgram<ConsulConfiguration>
    {
        /// <summary>
        /// Main function.
        /// Initializes the web service.
        /// </summary>
        public static void Main()
        {
            System.Net.ServicePointManager.DefaultConnectionLimit = int.MaxValue;

            InitializeWebService<Startup>();
        }
    }
}
