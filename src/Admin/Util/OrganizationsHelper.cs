﻿using Coscine.Database.DataModel;
using Coscine.Database.Models;
using Coscine.Metadata;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.Admin.Util;

internal class OrganizationsHelper
{
    private static readonly ExternalIdModel _externalIdModel = new();

    private static readonly RdfStoreConnector _rdfStoreConnector = new(Program.Configuration.GetStringAndWait("coscine/local/virtuoso/additional/url"));

    public static IEnumerable<string> GetOrganization(User user)
    {
        // Bellow code taken from Organizations API
        var externalIds = _externalIdModel.GetAllWhere((externalId) => externalId.UserId == user.Id);
        var externalIdList = _externalIdModel.GetAllWhere((externalId) => externalId.UserId == user.Id).Select(x => x.ExternalId1).ToList();
        var externalOrganizations = externalIds.Select((externalId) => externalId.Organization);

        var triplesExternalIds = _rdfStoreConnector.GetTriples(null, null, null, 1, externalIdList);

        var triplesExternalOrganizations = externalOrganizations.SelectMany(x => _rdfStoreConnector.GetOrganizationByEntityId(x));

        return triplesExternalIds.Select(x => x.Subject.ToString())
            .Concat(triplesExternalOrganizations.Select(x => x.Subject.ToString()))
            .Distinct();
    }
}